package com.cahyana;

import com.cahyana.model.Users;
import com.fasterxml.classmate.AnnotationConfiguration;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.sql.Timestamp;
import java.util.Date;

public class HibernateUtil {

    static Session session;
    static SessionFactory sessionFactory;

    private static SessionFactory buildSessionFactory() {
        Configuration config = new Configuration().configure();
        config.addAnnotatedClass(Users.class);
        config.configure("hibernate.cfg.xml");

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();
        sessionFactory = config.buildSessionFactory(serviceRegistry);
        return sessionFactory;
    }

    public static void createRecord() {
        Users user;
        try {
            session = buildSessionFactory().openSession();
            session.beginTransaction();

            for (int j=101; j<=105; j++) {
                user = new Users();
                user.setId(j);
                user.setName("Editor " + j);
                user.setCreatedBy("Administrator");
                user.setCreatedDate(new Timestamp(new Date().getTime()));

                session.save(user);
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (session.getTransaction() != null) {
                System.out.println("....Transaction is being rolled back....");
                session.getTransaction().rollback();
            }
            e.printStackTrace();
        } finally {
            if (session != null)
                session.close();
        }
        System.out.println("Record saved successfully in the database...");
    }

    public static void updateRecord() {
        Users users;
        int userId = 300;
        try {
            session = buildSessionFactory().openSession();
            session.beginTransaction();
            users = session.get(Users.class, userId);
            users.setName("jika nama ini di input ke database pasti bakalan error...");
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction() != null) {
                System.out.println("......Transaction is being rolled back......");
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}
